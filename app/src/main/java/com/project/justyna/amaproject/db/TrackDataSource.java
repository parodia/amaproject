package com.project.justyna.amaproject.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Justyna on 2014-11-28.
 */
public class TrackDataSource {

    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { MySQLiteHelper.COLUMN_TRACKID,
            MySQLiteHelper.COLUMN_NAME,
            MySQLiteHelper.COLUMN_TRACK,
            MySQLiteHelper.COLUMN_CREATED};


    public TrackDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
        Log.i("DB", ""+database);
    }

    public void close() {
        dbHelper.close();
    }



    public int insertTrack (Track track){
        ContentValues values = new ContentValues();
        //values.put(MySQLiteHelper.COLUMN_TRACKID, track.getTrack_id());
        values.put(MySQLiteHelper.COLUMN_NAME, track.getName());
        values.put(MySQLiteHelper.COLUMN_TRACK, track.getTrack());
        values.put(MySQLiteHelper.COLUMN_CREATED, track.getCreated());
        int insertId = (int)database.insert(MySQLiteHelper.TABLE_TRACKS, null, values);
        /*Cursor cursor = database.query(MySQLiteHelper.TABLE_TRACKS,
                allColumns, MySQLiteHelper.COLUMN_TRACKID + " = " + insertId, null,
                null, null, null);
        track.setTrack_id(insertId);
        cursor.moveToFirst();
        cursor.close();*/
        return insertId;
    }

    public List<Track> getTracks() {
        ArrayList tracks = new ArrayList<Track>();
        Cursor cursor = database.query(MySQLiteHelper.TABLE_TRACKS, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Track t = new Track(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));
            tracks.add(t);
            cursor.moveToNext();
        }
        cursor.close();
        return tracks;
    }


    public void printLocations(){
        Cursor cursor = database.query(MySQLiteHelper.TABLE_NOTES, null, null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isLast())
        {
            System.out.println(cursor.getInt(0));
            cursor.moveToNext();
        }
    }

    public int numberOfTracks() {
        Cursor cursor;
        try {
            cursor = database.rawQuery("select * from "+MySQLiteHelper.TABLE_TRACKS, null);
        } catch (SQLiteException e) {
            return 0;
        }
        return cursor.getCount();
    }

    public Track getTrack(int trackId) {
        Cursor cursor = database.query(MySQLiteHelper.TABLE_TRACKS, null, MySQLiteHelper.COLUMN_TRACKID+"=" +trackId, null, null, null, null);
        cursor.moveToFirst();
        Track track = new Track(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));
        cursor.close();
        return track;
    }

}