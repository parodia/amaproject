package com.project.justyna.amaproject.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Justyna on 2014-11-28.
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

    public static final String TABLE_NOTES = "notes";
    public static final String COLUMN_NOTEID = "noteId";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_LONGITUDE = "longitude";
    public static final String COLUMN_TEXT = "noteText";
    public static final String COLUMN_DATE = "dateSaved";
    public static final String COLUMN_PHOTO = "photo";
    public static final String COLUMN_TRACKID = "trackId";

    public static final String TABLE_TRACKS = "tracks";
    //public static final String COLUMN_TRACKID = "trackId";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_TRACK = "track";
    public static final String COLUMN_CREATED = "created";


    private static final String DATABASE_CREATE_TABLE_NOTES = "create table "
            + TABLE_NOTES + "("
            + COLUMN_NOTEID + " integer primary key autoincrement, "
            + COLUMN_LATITUDE + " float not null, "
            + COLUMN_LONGITUDE + " float not null, "
            + COLUMN_TEXT + " text, "
            //TODO ograniczenie na 200 znakow
            //TODO sprawidzic formaty
            + COLUMN_PHOTO + " blob, "
            + COLUMN_DATE + " text, "
            + COLUMN_TRACKID + " integer);";


    private static final String DATABASE_CREATE_TABLE_TRACKS = "create table "
            + TABLE_TRACKS  + "("
            + COLUMN_TRACKID + " integer primary key autoincrement, "
            + COLUMN_NAME + " text, "
            + COLUMN_TRACK + " text, "
            + COLUMN_CREATED + " text);";


    private static final String DATABASE_NAME = "locations.db";
    private static final int DATABASE_VERSION = 1;


    public MySQLiteHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //context.deleteDatabase(DATABASE_NAME);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        // TODO Auto-generated method stub

        database.execSQL(DATABASE_CREATE_TABLE_NOTES);
        database.execSQL(DATABASE_CREATE_TABLE_TRACKS);
    }

    public void restartDB(SQLiteDatabase database) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTES);
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_TRACKS);
        database.execSQL(DATABASE_CREATE_TABLE_NOTES);
        database.execSQL(DATABASE_CREATE_TABLE_TRACKS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i("upgrade",
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        if(newVersion == oldVersion + 1)
        {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTES);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRACKS);
        }
        onCreate(db);
    }
}
