package com.project.justyna.amaproject.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by j on 06.01.15.
 */
public class NoteDataSource {
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { MySQLiteHelper.COLUMN_NOTEID,
            MySQLiteHelper.COLUMN_LATITUDE,
            MySQLiteHelper.COLUMN_LONGITUDE,
            MySQLiteHelper.COLUMN_TEXT,
            MySQLiteHelper.COLUMN_DATE,
            MySQLiteHelper.COLUMN_PHOTO,
            MySQLiteHelper.COLUMN_TRACKID};

    public NoteDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
        Log.i("DB", "" + database);
    }

    public void close() {
        dbHelper.close();
    }

    public void insertNote (Note note){
        ContentValues values = new ContentValues();
        //values.put(MySQLiteHelper.COLUMN_NOTEID, note.getNoteId());
        values.put(MySQLiteHelper.COLUMN_LATITUDE, note.getLat());
        values.put(MySQLiteHelper.COLUMN_LONGITUDE, note.getLng());
        values.put(MySQLiteHelper.COLUMN_TEXT, note.getText());
        values.put(MySQLiteHelper.COLUMN_DATE, note.getDate());
        values.put(MySQLiteHelper.COLUMN_PHOTO, note.getPhoto());
        values.put(MySQLiteHelper.COLUMN_TRACKID, note.getTrackId());
        //database.insert(MySQLiteHelper.TABLE_NOTES, null, values);
        int insertId = (int)database.insert(MySQLiteHelper.TABLE_NOTES, null, values);
        /*Cursor cursor = database.query(MySQLiteHelper.TABLE_NOTES,
                allColumns, MySQLiteHelper.COLUMN_NOTEID + " = " + insertId, null,
                null, null, null);
        note.setNoteId(insertId);
        cursor.moveToFirst();
        cursor.close();*/
    }

    public List<Note> getNotes(){
        ArrayList notes = new ArrayList<Note>();
        Cursor cursor = database.query(MySQLiteHelper.TABLE_NOTES, null, null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Note n = new Note(cursor.getInt(0), cursor.getDouble(1), cursor.getDouble(2),
                    cursor.getString(3), cursor.getBlob(4), cursor.getString(5), cursor.getInt(6));
            notes.add(n);
            cursor.moveToNext();
        }
        cursor.close();
        return notes;
    }

    public List<Note> getNotes(int trackId){
        ArrayList notes = new ArrayList<Note>();
        Cursor cursor = database.query(MySQLiteHelper.TABLE_NOTES, null, MySQLiteHelper.COLUMN_TRACKID+"="+trackId, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Note n = new Note(cursor.getInt(0), cursor.getDouble(1), cursor.getDouble(2),
                    cursor.getString(3), cursor.getBlob(4), cursor.getString(5), cursor.getInt(6));
            notes.add(n);
            cursor.moveToNext();
        }
        cursor.close();
        return notes;
    }


    public void updateNotes (int trackId) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_TRACKID, trackId);
        database.update(MySQLiteHelper.TABLE_NOTES, values, MySQLiteHelper.COLUMN_TRACKID+"=0",null);
    }

    public Note getNote(int noteId){
        Cursor cursor = database.query(MySQLiteHelper.TABLE_NOTES, null, MySQLiteHelper.COLUMN_NOTEID+"="+noteId, null, null, null, null);
        cursor.moveToFirst();
        Note n = new Note(cursor.getInt(0), cursor.getDouble(1), cursor.getDouble(2),
            cursor.getString(3), cursor.getBlob(4), cursor.getString(5), cursor.getInt(6));
        cursor.close();
        return n;
    }

}
