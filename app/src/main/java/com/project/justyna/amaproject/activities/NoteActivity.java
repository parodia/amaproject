package com.project.justyna.amaproject.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.justyna.amaproject.R;
import com.project.justyna.amaproject.db.Note;
import com.project.justyna.amaproject.db.NoteDataSource;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.util.Calendar;


public class NoteActivity extends ActionBarActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    ImageView imageView;
    EditText editText;
    double[] loc;
    Bitmap imageBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        imageBitmap = Bitmap.createBitmap(100,100, Bitmap.Config.ALPHA_8);
        imageView = (ImageView)findViewById(R.id.imageView);
        Button button = (Button)findViewById(R.id.saveNoteButton);
        TextView textView = (TextView)findViewById(R.id.textView);
        editText = (EditText)findViewById(R.id.noteText);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });

        loc = getIntent().getDoubleArrayExtra("location");
        textView.setText(loc[0]+" "+loc[1]);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imageBitmap);
        }
    }

    private void save() {
        NoteDataSource noteDataSource = new NoteDataSource(this);
        try {
            noteDataSource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String created = Calendar.getInstance().getTime().toString();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
        byte[] img = bos.toByteArray();
        noteDataSource.insertNote(new Note(loc[0], loc[1], String.valueOf(editText.getText()), img, created));
        noteDataSource.close();

        Intent returnIntent = new Intent();
        returnIntent.putExtra("location",loc);
        setResult(RESULT_OK,returnIntent);
        finish();
    }

}
