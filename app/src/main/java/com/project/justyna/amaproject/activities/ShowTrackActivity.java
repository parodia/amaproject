package com.project.justyna.amaproject.activities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.project.justyna.amaproject.R;
import com.project.justyna.amaproject.db.Note;
import com.project.justyna.amaproject.db.NoteDataSource;
import com.project.justyna.amaproject.db.Track;
import com.project.justyna.amaproject.db.TrackDataSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ShowTrackActivity extends FragmentActivity {
    int trackId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_track);

        TrackDataSource trackDataSource = new TrackDataSource(this);
        NoteDataSource noteDataSource = new NoteDataSource(this);
        try {
            trackDataSource.open();
            noteDataSource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        trackId = getIntent().getIntExtra("trackId",-1);
        Track track = trackDataSource.getTrack(trackId);
        List<Note> notes = noteDataSource.getNotes(trackId);

        trackDataSource.close();
        noteDataSource.close();

        List<LatLng> points = PolyUtil.decode(track.getTrack());
        System.out.println(track.getTrack());
        System.out.println(points);
        Toast.makeText(this, "track "+trackId+" "+notes.size(), Toast.LENGTH_SHORT).show();

        //Toast.makeText(this, "track "+trackId, Toast.LENGTH_SHORT).show();


        final GoogleMap map;
        map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        map.setMyLocationEnabled(true);

        map.addPolyline(new PolylineOptions()
                .addAll(points)
                .width(5)
                .color(Color.RED));

        final HashMap<Marker, Note> hashMap = new HashMap<>();
        for (Note note: notes) {
            Marker m = map.addMarker(new MarkerOptions().position(new LatLng(note.getLat(), note.getLng())));

            hashMap.put(m,note);
        }



        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng p: points) {
            builder.include(p);
        }

        final LatLngBounds bounds = builder.build();
        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                int PADDING = 200;
                map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, PADDING));
                map.setOnCameraChangeListener(null);
            }
        });

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Intent intent = new Intent(ShowTrackActivity.this, ShowNoteActivity.class);
                intent.putExtra("noteId", hashMap.get(marker).getNoteId());
                intent.putExtra("trackId", trackId);
                startActivityForResult(intent, 0);
                return true;
            }
        });
        /*
        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                View view = getLayoutInflater().inflate(R.layout.note_bubble, null);
                Note note = hashMap.get(marker);

                ImageView imageView = (ImageView)view.findViewById(R.id.imageView);
                TextView textView = (TextView)view.findViewById(R.id.textView);
                byte[] img = note.getPhoto();


                imageView.setImageBitmap(BitmapFactory.decodeByteArray(img, 0, img.length));
                textView.setText(note.getText());
                return view;
            }
        });*/

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        trackId = intent.getIntExtra("trackId",-1);
        Toast.makeText(this, "trackId "+trackId,Toast.LENGTH_SHORT).show();
    }


}