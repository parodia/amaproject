package com.project.justyna.amaproject.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Justyna on 2014-11-27.
 */
public class Dialogs {
    static boolean consent = false;
    static String name = "";


    public void noGPS(Context ctx){
        final Context context = ctx;
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setTitle("GPS");
        alertDialog.setMessage("To use the app you need to turn on the GPS");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Go to settings",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No, exit the app",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int i) {
                        ((Activity)context).finish();
                        System.exit(0);
                    }
                });
        alertDialog.show();
    }


}