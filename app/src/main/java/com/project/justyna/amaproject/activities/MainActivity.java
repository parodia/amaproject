package com.project.justyna.amaproject.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.maps.android.PolyUtil;
import com.project.justyna.amaproject.R;
import com.project.justyna.amaproject.db.MySQLiteHelper;
import com.project.justyna.amaproject.db.Track;
import com.project.justyna.amaproject.db.TrackDataSource;

import java.sql.SQLException;
import java.util.Calendar;



public class MainActivity extends ActionBarActivity {

    Button button2;
    TrackDataSource trackDataSource;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        trackDataSource = new TrackDataSource(this);
        try {
            trackDataSource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Button button1 = (Button) findViewById(R.id.button);
        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick (View v) {
                Intent intent;
                intent = new Intent(MainActivity.this, TrackingActivity.class);
                //intent.putExtra("tracking","false");
                startActivity(intent);
            }
        });

        button2 = (Button) findViewById(R.id.button2);
        button2.setText("Display tracks (" + trackDataSource.numberOfTracks() + " )");
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick (View v) {
                Intent intent;
                intent = new Intent(MainActivity.this, DisplayTracksActivity.class);
                startActivity(intent);
            }
        });
        Button button3 = (Button)findViewById(R.id.button3);
        button3.setText("Generate initial data");
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trackDataSource.insertTrack(new Track("Track2", PolyUtil.decode("g_pgIivthBhg}AwsfQnheC}j_AnotDghjBhlrCnabDaab@pzgLeulB`lkH{srCren@ulwAgppA"), Calendar.getInstance().getTime().toString()));
            }
        });
        final Context context = this;
        Button button4 = (Button)findViewById(R.id.button4);
        button4.setText("Clear database");
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MySQLiteHelper sqLiteHelper = new MySQLiteHelper(context);
                sqLiteHelper.restartDB(sqLiteHelper.getWritableDatabase());
                Toast.makeText(context, "Database cleared", Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        button2.setText("Display tracks (" + trackDataSource.numberOfTracks() + " )");
        System.out.println("Main activity on resume");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_about) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}