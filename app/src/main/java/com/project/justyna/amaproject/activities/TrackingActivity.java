package com.project.justyna.amaproject.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.project.justyna.amaproject.R;
import com.project.justyna.amaproject.db.NoteDataSource;
import com.project.justyna.amaproject.db.TrackDataSource;
import com.project.justyna.amaproject.db.Track;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class TrackingActivity extends FragmentActivity implements LocationListener, SensorEventListener {
    protected int INTERVAL = 1000;
    protected int ACCURACY = 10;
    protected int ZOOM = 13;
    protected int SHAKING_ACCURACY = 10;
    protected int NOTE_LOCATION = 1;

    List<LatLng> points;
    Button button;
    private GoogleMap mMap;
    private boolean tracking = false;
    Location currentLocation;
    private SensorManager sensorManager;
    String trackName;

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "trackingActivity onStart", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);
        Toast.makeText(this, "trackingActivity onCreate", Toast.LENGTH_SHORT).show();


        //tracking = false;

        button = (Button) findViewById(R.id.buttonStartStop);
        button.setText("Start tracking");
        final Context ctx = this; //only to use in this stupid listener
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                builder.setMessage(!tracking ? "Do you want to start tracking?" : "Do you want to stop tracking?")
                        .setPositiveButton("Yes",  new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                if (!tracking)
                                    startTracking();
                                else stopTracking();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        //check();
        setUpMapIfNeeded();


        sensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Toast.makeText(this, "trackingActivity onResume", Toast.LENGTH_SHORT).show();
        //check();
        setUpMapIfNeeded();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this, "trackingActivity onStop", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "trackingActivity onDestroy", Toast.LENGTH_SHORT).show();
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Toast.makeText(this, "trackingActivity onPause", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        if (tracking) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("You will loose your track! Are you sure?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            mNotificationManager.cancel(0);
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        } else super.onBackPressed();
    }


    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            mMap.setMyLocationEnabled(true);

            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, INTERVAL, ACCURACY, this);
        currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (currentLocation != null) {
            onLocationChanged(currentLocation);
        }

    }

    private void drawPath() {
        mMap.addPolyline(new PolylineOptions()
                .addAll(points)
                .width(5)
                .color(Color.RED));
        Log.i("PATH", "drawPath");
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
        LatLng latlng = new LatLng(location.getLatitude(), location.getLongitude());
        Toast.makeText(this, latlng.toString(), Toast.LENGTH_LONG).show();
        if (tracking) {
            points.add(latlng);
            drawPath();
        }
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, ZOOM));

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Toast.makeText(this, "Provider " + provider + " changed - " + status, Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Enabled new provider " + provider, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Disabled provider " + provider, Toast.LENGTH_SHORT).show();

    }


    private void startTracking() {
        tracking = true;
        points = new ArrayList<>();
        if (currentLocation != null) {
            LatLng latlng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            points.add(latlng);
            Log.i("points.add", latlng + "");
        }
        Toast.makeText(this, "Start tracking...", Toast.LENGTH_SHORT).show();
        button.setText("Stop tracking");
        Notification n = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_tracking_icon)
                .setContentTitle("Tracking active").build();

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, n);


    }

    private void stopTracking() {
        tracking = false;
        button.setText("Start tracking");
        Log.i("tracking", "" + tracking);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(0);

        final TrackDataSource trackDataSource = new TrackDataSource(this);
        final NoteDataSource noteDataSource = new NoteDataSource(this);
        try {
            trackDataSource.open();
            noteDataSource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        int no = trackDataSource.numberOfTracks() + 1;
        trackName = "Track" + no;
        final EditText input = new EditText(this);
        input.setText(trackName);
        final String created = Calendar.getInstance().getTime().toString();
        final Context context = this;

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Save a new track")
                .setView(input)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        trackName = String.valueOf(input.getText());
                        int trackId = trackDataSource.insertTrack(new Track(trackName, points, created));
                        noteDataSource.updateNotes(trackId);
                        trackDataSource.close();
                        Toast.makeText(context, trackName + " saved.", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("Cancel", null)
                .setCancelable(false)
                .show();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float[] values = event.values;
        float x, y, z, accelationSquareRoot;
        if (/*tracking && */event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            x = values[0];
            y = values[1];
            z = values[2];
            accelationSquareRoot = (x * x + y * y + z * z) / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
            if (accelationSquareRoot >= SHAKING_ACCURACY) {
                if (currentLocation != null) {
                    System.out.println("accelationSquareRoot " + accelationSquareRoot);
                    Toast.makeText(this, "Device was shaked", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(this, NoteActivity.class);
                    double[] locationDbl = {currentLocation.getLatitude(), currentLocation.getLongitude()};
                    intent.putExtra("location", locationDbl);
                    sensorManager.unregisterListener(this);
                    startActivityForResult(intent, NOTE_LOCATION);
                } else {
                    Toast.makeText(this, "Shaking detected but location not recognized", Toast.LENGTH_SHORT).show();
                }

            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == NOTE_LOCATION && resultCode == RESULT_OK) {
            double[] loc = intent.getDoubleArrayExtra("location");
            mMap.addMarker(new MarkerOptions().position(new LatLng(loc[0], loc[1])).title("Note"));
        }
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }


}