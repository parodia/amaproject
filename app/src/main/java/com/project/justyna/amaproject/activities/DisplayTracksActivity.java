package com.project.justyna.amaproject.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.project.justyna.amaproject.R;
import com.project.justyna.amaproject.db.TrackDataSource;
import com.project.justyna.amaproject.db.Track;

import java.sql.SQLException;
import java.util.List;


public class DisplayTracksActivity extends ActionBarActivity {

    List<Track> tracks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_tracks);

        final ListView listView = (ListView) findViewById(R.id.listview);

        TrackDataSource trackDataSource = new TrackDataSource(this);
        try {
            trackDataSource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        tracks = trackDataSource.getTracks();

        ArrayAdapter<Track> adapter = new ArrayAdapter<Track>(this,
                android.R.layout.simple_list_item_1, tracks);
        listView.setAdapter(adapter);

        trackDataSource.close();
        final Context context = this;
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(DisplayTracksActivity.this, ShowTrackActivity.class);
                intent.putExtra("trackId", tracks.get(position).getTrackId());
                startActivity(intent);
            }
        });
    }

}
