package com.project.justyna.amaproject.activities;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.project.justyna.amaproject.R;
import com.project.justyna.amaproject.db.Note;
import com.project.justyna.amaproject.db.NoteDataSource;

import java.sql.SQLException;

public class ShowNoteActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_note);

        int noteId = getIntent().getIntExtra("noteId",0);
        Toast.makeText(this, ""+noteId, Toast.LENGTH_SHORT).show();

        NoteDataSource noteDataSource = new NoteDataSource(this);
        try {
            noteDataSource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Note note = noteDataSource.getNote(noteId);

        ImageView imageView = (ImageView)findViewById(R.id.imageView);
        TextView textView = (TextView)findViewById(R.id.textView);

        byte[] img = note.getPhoto();


        imageView.setImageBitmap(BitmapFactory.decodeByteArray(img, 0, img.length));
        textView.setText(note.getText());

    }

    @Override
    public void onBackPressed() {
        int trackId = getIntent().getIntExtra("trackId",0);
        Intent intent = new Intent(ShowNoteActivity.this,ShowTrackActivity.class);
        intent.putExtra("trackId",trackId);
        startActivity(intent);
        finish();
        //} else
        //super.onBackPressed();
    }



}
