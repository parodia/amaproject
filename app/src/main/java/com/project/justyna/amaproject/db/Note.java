package com.project.justyna.amaproject.db;

/**
 * Created by j on 06.01.15.
 */
public class Note {
    private int noteId;
    private double lat;
    private double lng;
    private String text;
    private byte[] photo;
    private String date;
    private int trackId;

    public Note (int note_id, double lat, double lng, String text, byte[] photo, String date, int trackId) {
        this.noteId = note_id;
        this.lat = lat;
        this.lng = lng;
        this.text = text;
        this.photo = photo;
        this.date = date;
        this.trackId = trackId;
    }

    public Note (double lat, double lng, String text, byte[] photo, String date) {
        this.noteId = 0;
        this.lat = lat;
        this.lng = lng;
        this.text = text;
        this.photo = photo;
        this.date = date;
        this.trackId = 0;
    }

    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getTrackId() {
        return trackId;
    }

    public void setTrackId(int trackId) {
        this.trackId = trackId;
    }
}
