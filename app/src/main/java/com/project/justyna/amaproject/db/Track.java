package com.project.justyna.amaproject.db;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;
import com.google.maps.android.PolyUtil;

/**
 * Created by Justyna on 2014-12-09.
 */
public class Track {
    private int trackId;
    private String name;
    private String track;
    private String created;

    public Track() {
        this.trackId = 0;
        this.name = "";
        this.track = "";
        this.created = "";
    }
    //TODO sprawdzic ktore wywalic
    public Track(int track_id, String name, String track, String created) {
        this.trackId = track_id;
        this.name = name;
        this.track = track;
        this.created = created;
    }

    public Track (String name, List<LatLng> points, String created) {
        this.trackId = 0;
        this.name = name;
        this.track = encodeTrack(points);
        this.created = created;
    }

    public int getTrackId() {
        return trackId;
    }

    public void setTrackId(int trackId) {
        this.trackId = trackId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTrack() {
        return track;
    }

    public void setTrack(String track) {
        this.track = track;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "[" + getTrackId() + "]" + name + "(" + created + ") " + getTrack();
    }

    public static String encodeTrack(List<LatLng> points) {
        return PolyUtil.encode(points);
    }
}
